﻿using Microsoft.AspNetCore.Mvc;
using ReportAPI.Services.Domain;

namespace ReportAPI.Controllers
{
    [Route("api/[controller]")]
    public class ReportController : Controller
    {
        private IReportService _reportService;

        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        //api/report/employees
        [HttpGet("{reportName}")]
        public ActionResult Get(string reportName)
        {
            var returnString = _reportService.GenerateReport(reportName);
            return File(returnString, System.Net.Mime.MediaTypeNames.Application.Octet, reportName + ".pdf");
        }

        //api/report/db/employeesdb
        [HttpGet("db/{reportName}")]
        public ActionResult GetFromDb(string reportName)
        {
            var returnString = _reportService.GenerateReportDb(reportName);
            return File(returnString, System.Net.Mime.MediaTypeNames.Application.Octet, reportName + ".pdf");
        }
    }
}