﻿using Dapper;
using Microsoft.Extensions.Options;
using Report.Data.Dtos;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ReportAPI.Repository
{
    public class ReportRepository : IReportRepository
    {
        private readonly string _connectionString;

        public ReportRepository(IOptions<DbConnection> config)
        {
            _connectionString = config.Value.ConnectionString;
        }
        public IEnumerable<UserDto> GetEmployees()
        {
            const string command = @"SELECT FirstName, LastName, Email, Phone FROM ReportServiceDb.dbo.MainEmployees";

            using IDbConnection db = new SqlConnection(_connectionString);
            return db.Query<UserDto>(command, commandType: CommandType.Text);
        }
    }
}