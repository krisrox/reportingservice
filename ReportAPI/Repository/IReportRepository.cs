﻿using System.Collections.Generic;
using Report.Data.Dtos;

namespace ReportAPI.Repository
{
    public interface IReportRepository
    {
        public IEnumerable<UserDto> GetEmployees();
    }
}