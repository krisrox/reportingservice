﻿using AspNetCore.Reporting;
using Report.Data.Dtos;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ReportAPI.Services
{
    public abstract class ReportService
    {
        public byte[] GenerateReport(string reportName)
        {
            var fileDirPath = Assembly.GetExecutingAssembly().Location.Replace("ReportAPI.dll", string.Empty);
            var rdlcFilePath = $"{fileDirPath}ReportFiles\\{reportName}.rdlc";
            
            var parameters = new Dictionary<string, string>();
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding.GetEncoding("windows-1252");
            var report = new LocalReport(rdlcFilePath);

            var userList = new List<UserDto>();
            userList.Add(new UserDto
            {
                FirstName = "Adam",
                LastName = "Kowalski",
                Email = "adam.kowalski@gmail.com",
                Phone = "660123321"
            });

            userList.Add(new UserDto
            {
                FirstName = "Natalia",
                LastName = "Lewandowska",
                Email = "lewandowska@gmail.com",
                Phone = "500233111"
            });

            userList.Add(new UserDto
            {
                FirstName = "Robert",
                LastName = "Lewandowski",
                Email = "legia_warszawa@gmail.com",
                Phone = "533222111"
            });

            report.AddDataSource("dsUsers", userList);

            var result = report.Execute(RenderType.Pdf, 1, parameters);
            return result.MainStream;
        }
    }
}
