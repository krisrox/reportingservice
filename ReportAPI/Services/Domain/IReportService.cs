﻿namespace ReportAPI.Services.Domain
{
    public interface IReportService
    {
        byte[] GenerateReport(string reportName);

        byte[] GenerateReportDb(string reportName);
    }
}
