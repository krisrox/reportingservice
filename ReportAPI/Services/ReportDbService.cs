﻿using AspNetCore.Reporting;
using ReportAPI.Repository;
using ReportAPI.Services.Domain;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using Report.Data.Dtos;

namespace ReportAPI.Services
{
    public class ReportDbService : ReportService, IReportService
    {
        private readonly IReportRepository _reportRepository;

        public ReportDbService(IReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
        }

        public byte[] GenerateReportDb(string reportName)
        {
            var fileDirPath = Assembly.GetExecutingAssembly().Location.Replace("ReportAPI.dll", string.Empty);
            var rdlcFilePath = $"{fileDirPath}ReportFiles\\{reportName}.rdlc";
            var parameters = new Dictionary<string, string>();

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding.GetEncoding("windows-1252");
            var report = new LocalReport(rdlcFilePath);

            var employees = _reportRepository.GetEmployees();
            var userList = employees.Select(emp => 
                new UserDto
                {
                    FirstName = emp.FirstName, 
                    LastName = emp.LastName, 
                    Email = emp.Email, 
                    Phone = emp.Phone
                }).ToList();

            report.AddDataSource("dsUsers", userList);

            var result = report.Execute(RenderType.Pdf, 1, parameters);
            return result.MainStream;
        }
    }
}